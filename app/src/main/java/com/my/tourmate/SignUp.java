package com.my.tourmate;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class SignUp extends AppCompatActivity {

    private EditText emailEt,nameEt,passwordEt;
    private Button signupBtn;
    private TextView signinTv;
    private String name,email,password;
    private FirebaseAuth auth;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        init();
        signin();

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = nameEt.getText().toString();
                email = emailEt.getText().toString();
                password = passwordEt.getText().toString();

                if (TextUtils.isEmpty(name)){
                    Toast.makeText(SignUp.this, "Please enter Name!", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(email)){
                    Toast.makeText(SignUp.this, "Please enter Email!", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(password)){
                    Toast.makeText(SignUp.this, "Please enter Password!", Toast.LENGTH_SHORT).show();
                }
                else if (password.length()<6){
                    Toast.makeText(SignUp.this, "Password should be 6 character long!", Toast.LENGTH_SHORT).show();
                }
                else{
                    signup(name,email,password);
                }
            }
        });



    }

    private void signup(final String name, final String email, String password) {
        auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    String userId = auth.getCurrentUser().getUid();

                    DatabaseReference dataref = reference.child("user").child(userId);


                    HashMap<String,Object> userInfo = new HashMap<>();

                    userInfo.put("name",name);
                    userInfo.put("email",email);
                    userInfo.put("Id",userId);

                    dataref.setValue(userInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(SignUp.this, "Successfully Added", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(SignUp.this,SignIn.class));
                            }

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(SignUp.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(SignUp.this,e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void signin() {
        signinTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUp.this,SignIn.class));
            }
        });
    }

    private void init() {
        emailEt = findViewById(R.id.emailET);
        nameEt = findViewById(R.id.nameET);
        passwordEt = findViewById(R.id.passwordET);
        signinTv= findViewById(R.id.signinTV);
        signupBtn = findViewById(R.id.signupBtn);
        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference();
    }
}
