package com.my.tourmate;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.my.tourmate.Fragment.Home;
import com.my.tourmate.Fragment.NearBy;
import com.my.tourmate.Fragment.Places;
import com.my.tourmate.Fragment.Weather;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FloatingActionButton floatBtn;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    NavigationView navigationView;
    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();



        FragmentTransaction home = getSupportFragmentManager().beginTransaction();
        home.replace(R.id.framelayout,new Home());
        toolbar.setTitle("Home");
        home.commit();
    }




    private void init() {
        drawerLayout = findViewById(R.id.drawerlayout);
        toolbar = findViewById(R.id.toolbar);
        navigationView = findViewById(R.id.navigationview);
        drawerLayout = findViewById(R.id.drawerlayout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawerOpen, R.string.drawerClose);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.weather:
                toolbar.setTitle("Weather");
                FragmentTransaction weather = getSupportFragmentManager().beginTransaction();
                weather.replace(R.id.framelayout,new Weather());
                weather.commit();
                drawerLayout.closeDrawers();
                break;
            case R.id.home:
                toolbar.setTitle("Home");
                FragmentTransaction home = getSupportFragmentManager().beginTransaction();
                home.replace(R.id.framelayout,new Home());
                home.commit();
                drawerLayout.closeDrawers();
                break;
            case R.id.places:
                toolbar.setTitle("Places");
                FragmentTransaction places = getSupportFragmentManager().beginTransaction();
                places.replace(R.id.framelayout,new Places());
                places.commit();
                drawerLayout.closeDrawers();
                break;
            case R.id.nearby:
                toolbar.setTitle("NearBy");
                FragmentTransaction nearby = getSupportFragmentManager().beginTransaction();
                nearby.replace(R.id.framelayout,new NearBy());
                nearby.commit();
                drawerLayout.closeDrawers();
                break;
            case R.id.logout:
                Intent intent = new Intent(MainActivity.this, SignIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
        }
        return false;
    }
}
