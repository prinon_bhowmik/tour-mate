package com.my.tourmate.Expense;

public class ModelExpense {

    private String id;
    private String exp_type;
    private String exp_date;
    private String exp_amount;

    public ModelExpense() {
    }

    public ModelExpense(String id, String exp_type, String exp_date, String exp_amount) {
        this.id = id;
        this.exp_type = exp_type;
        this.exp_date = exp_date;
        this.exp_amount = exp_amount;
    }

    public String getId() {
        return id;
    }

    public String getExp_type() {
        return exp_type;
    }

    public String getExp_date() {
        return exp_date;
    }

    public String getExp_amount() {
        return exp_amount;
    }
}
