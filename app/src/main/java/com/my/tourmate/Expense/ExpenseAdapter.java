package com.my.tourmate.Expense;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.FirebaseDatabase;
import com.my.tourmate.R;

import java.util.List;

public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.ViewHolder> {

    private List<ModelExpense> expenseList;
    private Context context;
    private AlertDialog.Builder alert;
    private String eventId;

    public ExpenseAdapter(List<ModelExpense> expenseList, Context context, String eventId) {
        this.expenseList = expenseList;
        this.context = context;
        this.eventId = eventId;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.expense_model,parent,false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final ModelExpense expense = expenseList.get(position);

        holder.exptypeId.setText(expense.getExp_type());
        holder.expdateId.setText(expense.getExp_date());
        holder.amountId.setText(expense.getExp_amount());

        holder.menuId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,holder.menuId);
                popupMenu.inflate(R.menu.custom_option);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){

                            case R.id.editId:

                                Intent intent = new Intent(context, Update_Expense.class);
                                intent.putExtra("eventId",eventId);
                                intent.putExtra("id",expense.getId());
                                intent.putExtra("type",expense.getExp_type());
                                intent.putExtra("date",expense.getExp_date());
                                intent.putExtra("amount",expense.getExp_amount());
                                context.startActivity(intent);
                                break;
                            case R.id.deleteId:
                                alert = new AlertDialog.Builder(context);
                                alert.setTitle("Delete");
                                alert.setMessage("Are You Sure!");
                                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        String Id = expense.getId();
                                        FirebaseDatabase.getInstance().getReference("tour_details").child(eventId)
                                                .child("expense").child(Id).removeValue();
                                        expenseList.remove(expense);
                                        notifyDataSetChanged();
                                        Toast.makeText(context, "Delete!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                AlertDialog alertDialog = alert.create();
                                alertDialog.show();
                                break;
                        }


                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView exptypeId,expdateId,amountId;
        private ImageView menuId;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            exptypeId = itemView.findViewById(R.id.exptypeID);
            expdateId = itemView.findViewById(R.id.expdateID);
            amountId = itemView.findViewById(R.id.amountID);
            menuId = itemView.findViewById(R.id.menuId);
        }
    }
}
