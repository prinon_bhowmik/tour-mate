package com.my.tourmate.Expense;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.my.tourmate.R;

public class Add_expense extends AppCompatActivity {

    private EditText exp_type,exp_date,amountId;
    private Button addexpBtn,backBtn,updateBtn;
    private DatabaseReference reference;
    private String id,type,date,amount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense);
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        init();

       addexpBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               type = exp_type.getText().toString();
               date = exp_date.getText().toString();
               amount = amountId.getText().toString();
               if (TextUtils.isEmpty(type)){
                   Toast.makeText(Add_expense.this, "Enter Expense Type.", Toast.LENGTH_SHORT).show();
               }
               else if (TextUtils.isEmpty(date)){
                   Toast.makeText(Add_expense.this, "Enter Expense Type.", Toast.LENGTH_SHORT).show();
               }
               else if (TextUtils.isEmpty(amount)){
                   Toast.makeText(Add_expense.this, "Enter Expense Type.", Toast.LENGTH_SHORT).show();
               }
               else{
                   addonDB(type,date,amount);
               }

           }
       });





    }

    private void addonDB(String type, String date, String amount) {

        String userId = reference.push().getKey();

        ModelExpense expense = new ModelExpense(userId,type,date,amount);
        reference.child(userId).setValue(expense).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(Add_expense.this, "Added", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Add_expense.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void init() {
        exp_date = findViewById(R.id.exp_date);
        exp_type = findViewById(R.id.exp_type);
        amountId = findViewById(R.id.amount);
        addexpBtn = findViewById(R.id.addexpBtn);
        backBtn = findViewById(R.id.backBtn);
        updateBtn = findViewById(R.id.updateBtn);
        reference = FirebaseDatabase.getInstance().getReference().child("tour_details").child(id).child("expense");
    }
}
