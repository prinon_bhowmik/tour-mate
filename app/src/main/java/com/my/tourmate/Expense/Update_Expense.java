package com.my.tourmate.Expense;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.my.tourmate.R;
import com.my.tourmate.Event.Tour_expense;

public class Update_Expense extends AppCompatActivity {

    private EditText exp_type,exp_date,amountId;
    private Button backBtn,updateBtn;
    private  String eventId,expId,type,date,amount;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update__expense);

        init();


        Intent intent = getIntent();
        eventId = intent.getStringExtra("eventId");
        expId = intent.getStringExtra("id");
        type = intent.getStringExtra("type");
        date = intent.getStringExtra("date");
        amount = intent.getStringExtra("amount");

        exp_type.setText(type);
        exp_date.setText(date);
        amountId.setText(amount);

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (TextUtils.isEmpty(type)){
                    Toast.makeText(Update_Expense.this, "Please enter Expense Type", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(date)){
                    Toast.makeText(Update_Expense.this, "Please enter Expense date", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(amount)){
                    Toast.makeText(Update_Expense.this, "Please enter Expense Amount", Toast.LENGTH_SHORT).show();
                }
                else{
                    DatabaseReference update = FirebaseDatabase.getInstance().getReference("tour_details").child(eventId)
                            .child("expense").child(expId);
                    String gettype = exp_type.getText().toString();
                    String getdate = exp_date.getText().toString();
                    String getamount = amountId.getText().toString();
                    ModelExpense expense = new ModelExpense(expId,gettype,getdate,getamount);
                    update.setValue(expense);
                    Toast.makeText(Update_Expense.this, "Update Complete!", Toast.LENGTH_SHORT).show();

                }
            }

        });
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Update_Expense.this, Tour_expense.class));
            }
        });
    }

    private void init() {
        exp_date = findViewById(R.id.exp_date);
        exp_type = findViewById(R.id.exp_type);
        amountId = findViewById(R.id.amount);
        backBtn = findViewById(R.id.backBtn);
        updateBtn = findViewById(R.id.updateBtn);
    }
}
