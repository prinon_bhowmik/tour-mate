package com.my.tourmate.Fragment;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.my.tourmate.Map.GetNearbyPlaces;
import com.my.tourmate.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class NearBy extends Fragment implements OnMapReadyCallback {

    private TextView showlocation;
    private FloatingActionButton floatBtn;
    private PlacesClient placesClient;
    private List<AutocompletePrediction> predictionList;
    private Location mylocation;
    private LocationCallback locationCallback;
    private GoogleMap map;
    private double latitude,longtitude;
    private int proximate = 10000;
    private String [] permission={Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION};

    private ImageButton searchBtn,searchmenu;
    private EditText searchEt;
    private View mapview;


    public NearBy() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_near_by, container, false);

        floatBtn = view.findViewById(R.id.floatBtn);
        searchBtn = view.findViewById(R.id.searchBtn);
        searchEt = view.findViewById(R.id.searchEt);
        searchmenu = view.findViewById(R.id.searchmenu);

        getActivity().getSupportFragmentManager();

        SupportMapFragment supportMapFragment =(SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        getUserPermission();
        currentLocation();


        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getplace = searchEt.getText().toString();
                List<Address> addressList = null;
                MarkerOptions usermarker = new MarkerOptions();

                if (!TextUtils.isEmpty(getplace)){
                    Geocoder geocoder = new Geocoder(getContext());
                    try {
                        addressList = geocoder.getFromLocationName(getplace,6);
                        if (addressList!=null){
                            for (int i= 0; i<addressList.size();i++){
                                Address userAddress = addressList.get(i);
                                LatLng latLng = new LatLng(userAddress.getLatitude(),userAddress.getLongitude());

                                usermarker.position(latLng);
                                usermarker.title(getplace);
                                usermarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
                                map.addMarker(usermarker);
                                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                map.animateCamera(CameraUpdateFactory.zoomTo(14));

                            }
                        }else{
                            Toast.makeText(getContext(), "Location not found!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(getContext(), "Please provide location name!", Toast.LENGTH_SHORT).show();
                }


            }
        });


        floatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentLocation();
            }
        });

        searchmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String hospital = "hospital";
                final Object transferData[] = new Object[2];
                final GetNearbyPlaces getNearbyPlaces = new GetNearbyPlaces();


                PopupMenu popupMenu = new PopupMenu(getContext(),view);
                MenuInflater inflater = popupMenu.getMenuInflater();
                inflater.inflate(R.menu.nearby_options,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.hospitalId:

                                String url = getURL(latitude,longtitude,hospital);
                                transferData[0]=map;
                                transferData[1] = url;
                                getNearbyPlaces.execute(transferData);
                                Toast.makeText(getContext(), "Hospital", Toast.LENGTH_SHORT).show();
                        }



                        return false;
                    }
                });
                popupMenu.show();
            }
        });


        return view;
    }

    private String getURL(double latitude, double longtitude, String nearby) {

        StringBuilder googleURL = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googleURL.append("location=" + latitude + "," + longtitude);
        googleURL.append("&radius=" + proximate);
        googleURL.append("&type=" + nearby);
        googleURL.append("&sensor=true");
        googleURL.append("&key" + "AIzaSyBeVCyDpSxYKoQGAmjEgFi1xakPty2S5fg");

        Log.d("MapShow","url=" + googleURL.toString());

        return googleURL.toString();
    }


    private void currentLocation() {
        FusedLocationProviderClient fusedLocationProviderClient = new FusedLocationProviderClient(getContext());
        Task location = fusedLocationProviderClient.getLastLocation();
        location.addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                Location curentlocation =(Location)  task.getResult();
                latitude = curentlocation.getLatitude();
                longtitude = curentlocation.getLongitude();
                map.addMarker(new MarkerOptions().position(new LatLng(latitude,longtitude)));
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(curentlocation.getLatitude(),curentlocation.getLongitude()),15));
            }
        });
    }

   /* private void TapLocation(final GoogleMap googleMap) {

        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                final LatLng taplocation = googleMap.getCameraPosition().target;
                showlocation.setVisibility(View.VISIBLE);
                showlocation.setText(getAddress(taplocation.latitude,taplocation.longitude));
                showlocation.postDelayed(new Runnable() {
                    public void run() {
                        showlocation.setVisibility(View.GONE);

                    }
                }, 3000);
            }
        });
    }*/

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        // TapLocation(googleMap);
        currentLocation();
       /* LatLng bd =new LatLng (23.753668,90.393011);
        // googleMap.addMarker(new MarkerOptions().position(bd).title("Destination").snippet(getAddress(bd.latitude,bd.longitude)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bd,15));
        if (ActivityCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);*/
    }

    private void getUserPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(ActivityCompat.checkSelfPermission(getContext(),Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(permission,0);
            }
        }

    }

    public String getAddress(double lat,double lon){
        String address = "";

        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());

        List<Address> addressList;

        try {
            addressList=geocoder.getFromLocation(lat,lon,1);
            if (addressList.size()>0){
                address=addressList.get(0).getAddressLine(0);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }


        return address;
    }


}
