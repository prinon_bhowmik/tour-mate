package com.my.tourmate.Fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.my.tourmate.Event.Add_tour;
import com.my.tourmate.Event.EventAdapter;
import com.my.tourmate.Event.Model_event;
import com.my.tourmate.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment {

    FloatingActionButton floatBtn;

    private RecyclerView eventrecyclerId;
    private List<Model_event> eventList;
    private EventAdapter adapter;
    private DatabaseReference reference;
    private String userId;


    public Home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);


        getActivity().setTitle("Home");
        floatBtn = view.findViewById(R.id.floatBtn);
        eventrecyclerId = view.findViewById(R.id.eventRecyclerID);
        eventList = new ArrayList<>();
        eventrecyclerId.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new EventAdapter(eventList,getContext());
        eventrecyclerId.setAdapter(adapter);
        reference = FirebaseDatabase.getInstance().getReference();


        getData();


        floatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity().getApplication(), Add_tour.class));
            }
        });

        return view;
    }

    private void getData() {

        DatabaseReference showref = reference.child("tour_details");

        showref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    eventList.clear();
                    for (DataSnapshot data : dataSnapshot.getChildren()) {

                        Model_event event = data.getValue(Model_event.class);
                        eventList.add(event);
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
