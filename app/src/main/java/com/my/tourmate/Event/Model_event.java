package com.my.tourmate.Event;

public class Model_event {

   private String id;
   private String tourname;
   private String description;
   private String budget;
   private String startdate;
   private String enddate;

    public Model_event() {
    }

    public Model_event(String id, String tourname, String description, String budget, String startdate, String enddate) {
        this.id = id;
        this.tourname = tourname;
        this.description = description;
        this.budget = budget;
        this.startdate = startdate;
        this.enddate = enddate;
    }

    public String getId() {
        return id;
    }

    public String getTourname() {
        return tourname;
    }

    public String getDescription() {
        return description;
    }

    public String getBudget() {
        return budget;
    }

    public String getStartdate() {
        return startdate;
    }

    public String getEnddate() {
        return enddate;
    }
}
