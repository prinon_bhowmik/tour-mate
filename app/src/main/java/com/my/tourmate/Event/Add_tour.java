package com.my.tourmate.Event;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.my.tourmate.MainActivity;
import com.my.tourmate.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Add_tour extends AppCompatActivity {

    private EditText placeEt,descriptionEt,startdateEt,enddateEt,budgetEt;
    private Button addtourBtn;
    private String userId,tourname,description,startdate,enddate,budget;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_tour);

        setTitle("Add Event");

        init();

        Intent intent = getIntent();
        userId=intent.getStringExtra("userId");

        addtourBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tourname = placeEt.getText().toString();
                description = descriptionEt.getText().toString();
                startdate = startdateEt.getText().toString();
                enddate = enddateEt.getText().toString();
                budget = budgetEt.getText().toString();

                if (TextUtils.isEmpty(tourname)){
                    Toast.makeText(Add_tour.this, "Please enter tour name", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(description)){
                    Toast.makeText(Add_tour.this, "Please enter tour description", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(startdate)){
                    Toast.makeText(Add_tour.this, "Please enter date of tour start", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(enddate)){
                    Toast.makeText(Add_tour.this, "Please enter date of tour end", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(budget)){
                    Toast.makeText(Add_tour.this, "Please enter tour budget", Toast.LENGTH_SHORT).show();
                }
                else{
                    addonDB(tourname,description,startdate,enddate,budget);
                }
            }
        });
    }

    private void addonDB(String tourname, String description, String startdate, String enddate, String budget) {

        String id = reference.push().getKey();
        Model_event event = new Model_event(id,tourname,description,budget,startdate,enddate);
        reference.child(id).setValue(event).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(Add_tour.this, "Event Created!", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Add_tour.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        startActivity(new Intent(Add_tour.this, MainActivity.class));
    }


    private void init() {
        placeEt = findViewById(R.id.placeEt);
        descriptionEt = findViewById(R.id.descriptionEt);
        startdateEt = findViewById(R.id.startdateEt);
        enddateEt = findViewById(R.id.enddateEt);
        budgetEt = findViewById(R.id.budgetEt);
        addtourBtn = findViewById(R.id.addtourBtn);
        reference = FirebaseDatabase.getInstance().getReference().child("tour_details");
    }

    public void startDateConfig(View view) {
        startdateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month = month+1;
                        String currentDate = year+"/"+month+"/"+day;
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                        Date date = null;

                        try {
                            date = dateFormat.parse(currentDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        startdateEt.setText(dateFormat.format(date));
                        long milisec = date.getTime();
                    }
                };

                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Add_tour.this, dateSetListener,year,month,day);
                datePickerDialog.show();
            }
        });
    }

    public void endDateConfig(View view) {
       enddateEt.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                   @Override
                   public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                       month = month+1;
                       String currentDate = year+"/"+month+"/"+day;
                       SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                       Date date = null;

                       try {
                           date = dateFormat.parse(currentDate);
                       } catch (ParseException e) {
                           e.printStackTrace();
                       }
                       enddateEt.setText(dateFormat.format(date));
                       long milisec = date.getTime();
                   }
               };

               Calendar calendar = Calendar.getInstance();
               int year = calendar.get(Calendar.YEAR);
               int month = calendar.get(Calendar.MONTH);
               int day = calendar.get(Calendar.DAY_OF_MONTH);

               DatePickerDialog datePickerDialog = new DatePickerDialog(Add_tour.this, dateSetListener,year,month,day);
               datePickerDialog.show();
           }
       });
    }
}
