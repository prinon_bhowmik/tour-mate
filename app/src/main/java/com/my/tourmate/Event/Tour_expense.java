package com.my.tourmate.Event;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.my.tourmate.Expense.Add_expense;
import com.my.tourmate.Expense.ExpenseAdapter;
import com.my.tourmate.Expense.ModelExpense;
import com.my.tourmate.R;

import java.util.ArrayList;
import java.util.List;

public class Tour_expense extends AppCompatActivity {

    private TextView placeId,budgetId;
    private FloatingActionButton addexpense;
    private RecyclerView expenserecycler;
    private List<ModelExpense> expenseList;
    private ExpenseAdapter adapter;
    private DatabaseReference reference;
    private String eventid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_expense);
        setTitle("Tour Expense");

        Intent intent = getIntent();
        String place = intent.getStringExtra("tourname");
        eventid = intent.getStringExtra("id");
        String budget = intent.getStringExtra("budget");
        init();





        placeId.setText(place);
        budgetId.setText(budget);

        addexpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Tour_expense.this, Add_expense.class);
                i.putExtra("id",eventid);
                startActivity(i);
            }
        });

        getData();
    }

    private void getData() {

        DatabaseReference showref = reference.child("expense");

        showref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    expenseList.clear();
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        ModelExpense event = data.getValue(ModelExpense.class);
                        if (event!=null){
                            expenseList.add(event);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void init() {
        placeId = findViewById(R.id.placeId);
        budgetId = findViewById(R.id.budgetId);
        addexpense = findViewById(R.id.addExpense);
        expenserecycler = findViewById(R.id.expenserecycler);
        expenserecycler.setLayoutManager(new LinearLayoutManager(this));
        expenseList = new ArrayList<>();
        adapter = new ExpenseAdapter(expenseList,this,eventid);
        expenserecycler.setAdapter(adapter);
        reference = FirebaseDatabase.getInstance().getReference("tour_details").child(eventid);
    }
}
