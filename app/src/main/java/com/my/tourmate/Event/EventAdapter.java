package com.my.tourmate.Event;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.my.tourmate.R;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private List<Model_event> eventList;
    private Context context;
    private AlertDialog.Builder alert;

    public EventAdapter(List<Model_event> eventList, Context context) {
        this.eventList = eventList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_model,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final Model_event event = eventList.get(position);

        holder.placeId.setText(event.getTourname());
        holder.descriptionId.setText(event.getDescription());
        holder.budgetId.setText(event.getBudget());
        holder.startdate.setText(event.getStartdate());
        holder.enddate.setText(event.getEnddate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,Tour_expense.class);
                intent.putExtra("id",event.getId());
                intent.putExtra("tourname",event.getTourname());
                intent.putExtra("description",event.getDescription());
                intent.putExtra("budget",event.getBudget());
                intent.putExtra("startdate",event.getStartdate());
                intent.putExtra("enddate",event.getEnddate());
                context.startActivity(intent);
            }
        });

        holder.menuId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                PopupMenu popup = new PopupMenu(context,holder.menuId);
                popup.inflate(R.menu.custom_option);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){

                            case R.id.editId:
                                Intent intent = new Intent(context,Update_event.class);
                                intent.putExtra("id",event.getId());
                                intent.putExtra("tourname",event.getTourname());
                                intent.putExtra("description",event.getDescription());
                                intent.putExtra("budget",event.getBudget());
                                intent.putExtra("startdate",event.getStartdate());
                                intent.putExtra("enddate",event.getEnddate());
                                context.startActivity(intent);
                                break;
                              case R.id.deleteId:
                                 alert = new AlertDialog.Builder(context);
                                 alert.setTitle("Delete!");
                                 alert.setMessage("Are you sure?");

                                 alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                     @Override
                                     public void onClick(DialogInterface dialogInterface, int i) {
                                         DatabaseReference reference = FirebaseDatabase.getInstance()
                                                 .getReference("tour_details").child(event.getId());
                                         reference.removeValue();
                                         Toast.makeText(context, "Delete", Toast.LENGTH_SHORT).show();
                                     }
                                 });
                                  alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(DialogInterface dialogInterface, int i) {
                                          dialogInterface.cancel();
                                      }
                                  });
                                  AlertDialog alertDialog = alert.create();
                                  alertDialog.show();
                                 break;
                        }


                        return false;
                    }
                });
                popup.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView placeId,descriptionId,budgetId,startdate,enddate;
        private ImageView menuId;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            placeId = itemView.findViewById(R.id.placeId);
            descriptionId = itemView.findViewById(R.id.descriptionId);
            budgetId = itemView.findViewById(R.id.budgetId);
            startdate = itemView.findViewById(R.id.startdate);
            enddate = itemView.findViewById(R.id.enddate);
            menuId = itemView.findViewById(R.id.menuId);
        }

    }
}
