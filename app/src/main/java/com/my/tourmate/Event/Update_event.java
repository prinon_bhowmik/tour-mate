package com.my.tourmate.Event;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.my.tourmate.MainActivity;
import com.my.tourmate.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Update_event extends AppCompatActivity {

    private EditText placeEt,descriptionEt,startdateEt,enddateEt,budgetEt;
    private Button updateBtn;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_event);

        init();

        updateData();
    }

    private void updateData() {
        final String userId,tourname1,description1,budget1,startdate1,enddate1;
        Intent intent = getIntent();

        userId = intent.getStringExtra("id");
        tourname1 = intent.getStringExtra("tourname");
        budget1 = intent.getStringExtra("budget");
        description1 = intent.getStringExtra("description");
        startdate1 = intent.getStringExtra("startdate");
        enddate1 = intent.getStringExtra("enddate");

        placeEt.setText(tourname1);
        budgetEt.setText(budget1);
        descriptionEt.setText(description1);
        startdateEt.setText(startdate1);
        enddateEt.setText(enddate1);


        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (TextUtils.isEmpty(tourname1)){
                    Toast.makeText(Update_event.this, "Please enter tour name", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(description1)){
                    Toast.makeText(Update_event.this, "Please enter tour description", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(startdate1)){
                    Toast.makeText(Update_event.this, "Please enter date of tour start", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(enddate1)){
                    Toast.makeText(Update_event.this, "Please enter date of tour end", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(budget1)){
                    Toast.makeText(Update_event.this, "Please enter tour budget", Toast.LENGTH_SHORT).show();
                }
                else{
                    DatabaseReference update = FirebaseDatabase.getInstance().getReference("tour_details").child(userId);
                    String uname = placeEt.getText().toString();
                    String udescription = descriptionEt.getText().toString();
                    String ubudget = budgetEt.getText().toString();
                    String ustartdate = startdateEt.getText().toString();
                    String uenddate = enddateEt.getText().toString();
                    Model_event event = new Model_event(userId,uname,udescription,ubudget,ustartdate,uenddate);
                    update.setValue(event);
                    Toast.makeText(Update_event.this, "Update Complete!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Update_event.this, MainActivity.class));
                    finish();
                }
            }
        });
    }

    private void init() {
        placeEt = findViewById(R.id.placeEt);
        descriptionEt = findViewById(R.id.descriptionEt);
        startdateEt = findViewById(R.id.startdateEt);
        enddateEt = findViewById(R.id.enddateEt);
        budgetEt = findViewById(R.id.budgetEt);
        updateBtn = findViewById(R.id.updateBtn);
        reference = FirebaseDatabase.getInstance().getReference().child("tour_details");
    }


    public void startDateConfig(View view) {
        startdateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month = month+1;
                        String currentDate = year+"/"+month+"/"+day;
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                        Date date = null;

                        try {
                            date = dateFormat.parse(currentDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        startdateEt.setText(dateFormat.format(date));
                        long milisec = date.getTime();
                    }
                };

                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Update_event.this, dateSetListener,year,month,day);
                datePickerDialog.show();
            }
        });
    }

    public void endDateConfig(View view) {
        enddateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        month = month+1;
                        String currentDate = year+"/"+month+"/"+day;
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                        Date date = null;

                        try {
                            date = dateFormat.parse(currentDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        enddateEt.setText(dateFormat.format(date));
                        long milisec = date.getTime();
                    }
                };

                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Update_event.this, dateSetListener,year,month,day);
                datePickerDialog.show();
            }
        });
    }
}
